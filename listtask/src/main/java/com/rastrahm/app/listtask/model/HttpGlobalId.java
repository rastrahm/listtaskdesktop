/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rastrahm.app.listtask.model;

import com.rastrahm.app.listtask.model.HttpHead;

/**
 * Variable global que contiene los datos del Head cuando se utiliza la interfaz web
 * @author Rolando Strahm
 */
public class HttpGlobalId {
    public static HttpHead head;

    /**
     * Contructor en blanco
     */
    public HttpGlobalId() {
    }

    /**
     * Retorna el objeto HttpHead
     * @return HttpHead
     * @see com.rastrahm.app.listtask.model.HttpHead
     */
    public static HttpHead getHead() {
        return head;
    }

    /**
     * Coloca el objeto HttpHead para su disponibilidad global
     * @param head : HttpHead 
     * @see com.rastrahm.app.listtask.model.HttpHead
     */
    public static void setHead(HttpHead head) {
        HttpGlobalId.head = head;
    }
    
}
