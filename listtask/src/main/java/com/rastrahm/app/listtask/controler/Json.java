package com.rastrahm.app.listtask.controler;

/**
 * Clase que maneja la estructura y conversión de los JSON a objetos
 * 
 * @author Rolando Strahm
 *
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import com.rastrahm.app.listtask.model.Groups;
import com.rastrahm.app.listtask.model.HttpHead;
import javax.swing.JTextArea;

/**
 * Clase de manejo de los JSON con Google GSON
 * @author Rolando Strahm
 */
public class Json {
    /**
     * Lee un string json y lo transforma en objetos
     * @param string : String con el contenido del json
     * @return Groups : Objeto de grupos
     * @see com.rastrahm.app.listtask.model.Groups
     */
    public Groups fromJson(String string) {
        final Gson gson = new GsonBuilder().create();
        return gson.fromJson(string, Groups.class);
    }

    /**
     * Lee el objeto Groups y lo transforma en un string
     * @param group: Groups contenido para transformarlo en un JSON
     * @return String: Contenido del JSON
     * @see com.rastrahm.app.listtask.model.Groups
     */
    public String toJson(Groups group) {
        final Gson gson = new GsonBuilder().create();
        return gson.toJson(group);
    }
    
    /**
     * Retorna un objeto json a partir del texto guardado en el TextArea
     * @param save : jTextArea que contiene el texto de json
     * @return Groups : Objeto de grupos
     * @see com.rastrahm.app.listtask.model.Groups 
     */
    public Groups fromJson(JTextArea save) {
        final Gson gson = new GsonBuilder().create();
        return gson.fromJson(save.getText(), Groups.class);
    }
    
    /**
     * General el objeto HttpHead
     * @param string : String que contiene el json
     * @return HttpHead : Objeto HttpHead
     * @see com.rastrahm.app.listtask.model.HttpHead
     */
    public HttpHead fromJsonJWT(String string) {
        final Gson gson = new GsonBuilder().create();
        return gson.fromJson(string, HttpHead.class);
    }
}
