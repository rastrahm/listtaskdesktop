/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rastrahm.app.listtask.model;

/**
 * Super Clase (Clase Global) Guarda el objeto para verificar su edición
 * @author Rolando Strahm
 */
public class EditNewOriginal {
    
    static String Group;
    static String Value;

    /**
     * Contructor en blanco
     */
    public EditNewOriginal() {
    }

    /**
     * Retorna el String del grupo
     * @return String : Grupo
     */
    public static String getGroup() {
        return Group;
    }

    /**
     * Retorna el String del valor
     * @return String : Valor del grupo
     */
    public static String getValue() {
        return Value;
    }

    /**
     * Coloca el String del grupo
     * @param Group : String grupo
     */
    public static void setGroup(String Group) {
        EditNewOriginal.Group = Group;
    }

    /**
     * Coloca el valor del grupo
     * @param Value : String valor del grupo
     */
    public static void setValue(String Value) {
        EditNewOriginal.Value = Value;
    }
   
}
