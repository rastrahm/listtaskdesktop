package com.rastrahm.app.listtask.model;

import java.util.List;

/**
 * Clase que engloba a las tareas en grupos
 * @author Rolando Strahm
 */

public class TaskGroup {
	
    private int id;
    private String name;
    private List<Tasks> task;

    /**
     * Constructor sin parametros
     */
    public TaskGroup() {
        super();
    }

    /**
     * Contructor de grupos
     * @param id : int que contiene el Id del grupo
     * @param name : String nombre del grupo
     * @param tasks : List de GroupTask que contiene las tareas
     * @see com.rastrahm.app.listtask.model.Tasks
     */
    public TaskGroup(int id, String name, List<Tasks> tasks) {
        super();
        id = id;
        name = name;
        task = tasks;
    }

    /**
     * Retorna el Id del grupo
     * @return int con el Id del grupo
     */
    public int getId() {
        return id;
    }

    /**
     * Pone el id del grupo
     * @param id : int con el Id del grupo
     */
    public void setId(int id) {
        id = id;
    }

    /**
     * Devuelve el nombre del grupo
     * @return String : Nombre del grupo
     */
    public String getName() {
        return name;
    }

    /**
     * Pone el nombre del grupo
     * @param name : String con el nombre del grupo
     */
    public void setName(String name) {
        name = name;
    }

    /**
     * Vector que contiene todas las tareas del grupo
     * @return List GroupTask : Retorna el conjunto de tareas 
     * @see com.rastrahm.app.listtask.model.Tasks
     */
    public List<Tasks> getTasks() {
        return task;
    }

    /**
     * Pone el vector del conjunto de tareas
     * @param tasks : List GroupTask Vector con el contenido de tareas
     * @see com.rastrahm.app.listtask.model.Tasks
     */
    public void setTasks(List <Tasks> tasks) {
        task = tasks;
    }

    /**
     * Retorna un String con todo el contenido del grupo de tareas
     * @return String: String que contiene todo el contenido del taskGroup
     */
    @Override
    public String toString() {
        String strValues = null;
        for (Tasks tarea : this.task) {
            strValues += tarea.toString() + ", ";
        }
        return "Id= " + this.id + ", Name= " + this.name + ", Tasks= " +  strValues;
    }
    
    /**
     * Retorna el nuevo Id
     * @return int : id de la siguiente tarea
     */
    public int newId() {
        return task.size() + 1;
    }
    
    /**
     * Agrega una tarea a las tareas del grupo
     * @param tasks : Objeto Tasks para agregar
     * @see com.rastrahm.app.listtask.model.Tasks
     */
    public void addTask(Tasks tasks) {
        task.add(tasks);
    }
    
    /**
     * Borra una tarea del grupo
     * @param name : String con el valor de la tarea a borrar
     */
    public void removeTask (String name) {
        int i = 0;
        for (Tasks list : task) {
            if (list.getName().equals(name)) {
                task.remove(i);
            }
            i++;
        }
    }
}
