package com.rastrahm.app.listtask.controler;

import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JList;
import com.rastrahm.app.listtask.controler.Config;
import com.rastrahm.app.listtask.controler.FileUtils;
import com.rastrahm.app.listtask.controler.Json;
import com.rastrahm.app.listtask.controler.HTTP;
import com.rastrahm.app.listtask.model.Groups;
import com.rastrahm.app.listtask.model.TaskGroup;
import com.rastrahm.app.listtask.model.Tasks;
import com.rastrahm.app.listtask.model.HttpGlobalId;
import com.rastrahm.app.listtask.model.HttpHead;
import java.util.List;
import javax.swing.DefaultListModel;

/**
 * Clase que contiene las utilidades
 * @author Rolando Strahm
 */

public class Util {
    
    Config Conf = new Config("src/main/java/com/rastrahm/app/listtask/resource/listtask.properties");
    
    /**
     * Busca un String "search" en un array de String "values" y retorna la posición en caso de encontrarlo, utiliza indexOf asi que se comporta
     * como "que contiene a"
     * @param search : String a buscar
     * @param values : Array de String donde se buscara
     * @return int : -1: no encontro la cadena; 0: o mayor, 1ra posición del String dentro del array
     */
    public static int arraySearchPos(String search, String[] values) {
        int result = -1;
        for (int i = 0; i < values.length; i++) {
            if (values[i].indexOf(search) > 0) {
                result = i;
                break;
            }
        }
        return result;
    }

    /**
     * Carga el json que contiene el objeto Groups, según el caso de Archivo (propiedad file), 
     * o a través de la Web (propiedad url) según la propiedad method
     * @return Groups : Grupos contenidos en el json
     */
    public Groups loadGroup() {    
	Json jsonUtils = new Json();
        String json = null;
        if (Conf.getProperty("method").equals("file")) {
            FileUtils Read = new FileUtils();
            String file = Conf.getProperty("file");
            json = Read.Read(System.getProperty("user.dir") + "/src/main/java/com/rastrahm/app/listtask/resource/" + file);
        } else {
            String pass = "{\"user\" : \"" + Conf.getProperty("user") + "\", \"pass\" : \"" + Conf.getProperty("pass") + "\"}";
            String jwt = HTTP.sendPOST(Conf.getProperty("url") + "/listtask", pass, null);
            HttpHead httpHead = jsonUtils.fromJsonJWT(jwt);
            com.rastrahm.app.listtask.model.HttpGlobalId.setHead(httpHead);
            json = HTTP.sendGET(Conf.getProperty("url") + "/listtaskjson", httpHead.getJwt()).replace("\\", "");
            json = json.substring(1, json.length());
            json = json.substring(0, json.length() - 1);
        }
	Groups tasks = jsonUtils.fromJson(json);
        com.rastrahm.app.listtask.model.GlobalGroups.setGlobal(tasks);
        return tasks;
    }
    
    /**
     * Carga el combo box
     * @param combo: JComboBox que puestra los grupos
     * @param tasks: Groups con que se llena el ComboBox
     */
    public void fillCombo(JComboBox combo, Groups tasks) {
        List <TaskGroup> list = tasks.getGroup();
        for (int i = 0; i < list.size(); i++) {
            combo.addItem(list.get(i).getName());
        }
    }
    
    /**
     * Carga el JList con las tareas
     * @param list : JList que se cargara
     * @param combo : JComboBox que identifica el grupo a cargar
     * @param tasks : Groups que guarda la informacións de grupos y tareas
     */
    public void fillList(JList list, JComboBox combo, Groups tasks) {
        String search = combo.getSelectedItem().toString();
        List<Tasks> lists =  searchTasks(search, tasks);
        DefaultListModel listModel = new DefaultListModel();
        for (Tasks value : lists) {
            //listModel.add(value.getId(), value.getName());
            listModel.addElement(value.getName());
        }
        list.setModel(listModel);
    }
    
    /**
     * Busca una tarea
     * @param search : String cadena a buscar
     * @param tasks : Grupo donde se busca
     * @return List Task : Lista de tareas resultante
     */
    public List<Tasks> searchTasks(String search, Groups tasks) {
        List <TaskGroup> lists = tasks.getGroup();
        for (int i = 0; i < lists.size(); i++) {
            if (lists.get(i).getName().equals(search)) {
                List <Tasks> list = lists.get(i).getTasks();
                return list;
            }
        }
        return null;
    }
    
    /**
     * Busca un grupo dentro del objeto global GlobalGroup
     * @param search : String con la cadena a buscar
     * @return int : Número del grupo buscado
     */
    public int searchGroup(String search) {
        int pos = -1;
        //Recorrer el Global para buscar el TaskGroup
        for (TaskGroup task : com.rastrahm.app.listtask.model.GlobalGroups.getGlobal().getGroup()) {
            if (task.getName().equals(search)) {
                pos = task.getId();
                break;
            }
        }
        return pos;
    }
    
    /**
     * Busca un string dentro del objeto Groups
     * @param search : String a buscar
     * @param task : Groups donde se busca
     * @return String [] : Array que contiene el nombre del grupo y el valor de la tarea
     */
    public String[] searchString(String search, Groups task) {
        String [] response = new String[2];
        for (TaskGroup group : task.getGroup()) {
            if (group.getName().indexOf(search) > 0) {
                response[0] = group.getName();
                response[1] = "";
                break;
            } else {
                for (Tasks tasks : group.getTasks()) {
                    if (tasks.getName().indexOf(search) > 0) {
                        response[0] = group.getName();
                        response[1] = tasks.getName();
                        break;
                    }
                }
            }
        }
        return response;
    }
    
    /**
     * Devuelve el contenido seleccionado del ComboBox
     * @param combo : JComboBox 
     * @return String : Valor seleccionado
     */
    public String getGroup(JComboBox combo) {
        return combo.getSelectedItem().toString();
    }
    
    /**
     * Retorna el valor seleccionado dentro del JList
     * @param list : JList
     * @return String : Valor seleccionado dentro del JList
     */
    public String getTask(JList list) {
        return list.getSelectedValue().toString();
    }
    
    /**
     * Guarda el cotendio de la clase global GroupsGlobal, en el archivo (siempre) y la web (popiedad activa)
     * @return boolean : True guardo en el servidor, False error en alguno de los parametros
     */
    public boolean saveJson() {
        Json jsonUtils = new Json();
        if (!Conf.getProperty("method").equals("file")) {
            String pass = "{\"user\" : \"" + Conf.getProperty("user") + "\", \"pass\" : \"" + Conf.getProperty("pass") + "\"}";
            String jwt = HTTP.sendPOST(Conf.getProperty("url") + "/listtask", pass, null);
            HttpHead httpHead = jsonUtils.fromJsonJWT(jwt);
            String response = HTTP.sendPOST(Conf.getProperty("url") + "/listtaskjson", jsonUtils.toJson(com.rastrahm.app.listtask.model.GlobalGroups.getGlobal()), httpHead.getJwt());
        }
        String file = Conf.getProperty("file");
        if (com.rastrahm.app.listtask.controler.FileUtils.Writer(System.getProperty("user.dir") + "/src/main/java/com/rastrahm/app/listtask/resource/", file, jsonUtils.toJson(com.rastrahm.app.listtask.model.GlobalGroups.getGlobal()))) {
            return true;
        }
        return false;
    }
    
    /**
     * Retorna una propiedad de forma universal 
     * @param prop : String con la clave de la propiedad
     * @return String con el valor de la propiedad
     */
    public String getData(String prop) {
	return Conf.getProperty(prop);
    }
    
    /**
     * Coloca un valor dentro de la propiedad
     * @param key : String con el clave a guardar
     * @param value : String con el valor de la propiedad
     */
    public void setData(String key, String value) {
	Conf.setProperty(key, value);
    }
    
    /**
     * Guarda las propiedades en el archivo de configuración
     */
    public void saveData() {
        Conf.saveProperty("src/main/java/com/rastrahm/app/listtask/resource/listtask.properties");
    }
}
